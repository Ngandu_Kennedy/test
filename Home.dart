import 'dart:core';

void main() {
  /*
  for (int x = 1; x < 5; x++) {
    print('$x Hello ');
  }
  ;
  var name = 3;
  print(name);
  print(reflect(name).type.reflectedType);
List<String> names = ['hulk', 'natacha', 'iron-man', 'spider-man'];
  names.forEach((name) {
    print(name.toUpperCase());
  });*/

  var avengerNames = <String>{'Hulk', 'Captain America'};
  var avengerQuotes = <String, String>{
    "Captain America": "I can do it all day",
    "Spider-Man": "Am i an Avenger?",
    "Hulk": "Smaaaaaah!"
  };
  avengerQuotes.forEach((key, value) {
    print('-$key: "$value"');
  });
}
terminal:
  # This can be any image that has the necessary runtime environment for your project.
  image: node:10-alpine
  before_script:
    - apk update
  script: sleep 60
  variables:
    RAILS_ENV: "test"
    NODE_ENV: "test"
